package modelo;

public class Alumno extends Persona {

	private int legajo;
	
	public Alumno() {
		
	}

	public Alumno(String nombre, String apellido, int legajo) {
		super(nombre, apellido);
		this.legajo = legajo;
	}

	public int getLegajo() {
		return legajo;
	}

	public void setLegajo(int legajo) {
		this.legajo = legajo;
	}
	
	public int hashCode()
	{
		return super.hashCode() + this.legajo;
	}
	
	public String toString()
	{
		return "\nNombre -> " + this.getNombre() + ", apellido -> "  + this.getApellido() + ", LEGAJO -> " + this.legajo;
	}
	
	public boolean equals(Object object)
	{
		boolean esIgual = false;
				
		if(object instanceof Alumno)
		{
			Alumno alumno = (Alumno) object;
			if(this.getNombre().equalsIgnoreCase(alumno.getNombre()) && this.getApellido().equalsIgnoreCase(alumno.getApellido()) && alumno.getLegajo() == this.legajo )
			{
				esIgual = true;
			}
		}
				
		return esIgual;
	}

}
