package modelo.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import modelo.Alumno;
import modelo.Persona;
import modelo.Profesor;

class PersonaTest {

	Persona personaVacio;
	Persona personaLleno;
	Persona alumno;
	Persona profesor;
	List<Persona> listaPersonas;
	Set<Persona> setPersonas;
	
	@BeforeEach
	void setUp() throws Exception {
		
		personaVacio = new Persona();
		personaLleno = new Persona("nombreTest", "apellidoTest");
		alumno = new Alumno("nombreAlumnoTest", "apellidoAlumnoTest", 1);
		profesor = new Profesor("nombreProfesorTest", "apellidoProfesorTest", "iosfaTest");
		
		listaPersonas = new ArrayList<>();
		listaPersonas.add(new Alumno());
		listaPersonas.add(new Profesor());
		
		listaPersonas.add(new Alumno("nombreAlumnoTest2", "apellidoAlumnoTest2", 2));
		listaPersonas.add(new Alumno("nombreAlumnoTest3", "apellidoAlumnoTest3", 3));
		listaPersonas.add(new Alumno("nombreAlumnoTest3", "apellidoAlumnoTest2", 3));
		
		listaPersonas.add(new Profesor("nombreProfesorTest2", "apellidoProfesorTest2", "iosfaTest2"));
		listaPersonas.add(new Profesor("nombreProfesorTest3", "apellidoProfesorTest3", "iosfaTest3"));
		listaPersonas.add(new Profesor("nombreProfesorTest4", "apellidoProfesorTest4", "iosfaTest4"));
		
		
		setPersonas = new HashSet<>();
		setPersonas.add(new Alumno());
		setPersonas.add(new Profesor());
		
		setPersonas.add(new Alumno("nombreAlumnoTest2", "apellidoAlumnoTest2", 2));
		setPersonas.add(new Alumno("nombreAlumnoTest3", "apellidoAlumnoTest3", 3));
		setPersonas.add(new Alumno("nombreAlumnoTest4", "apellidoAlumnoTest4", 4));
		
		setPersonas.add(new Profesor("nombreProfesorTest", "apellidoProfesorTest", "iosfaTest2"));
		setPersonas.add(new Profesor("nombreProfesorTest", "apellidoProfesorTest", "iosfaTest3"));
		setPersonas.add(new Profesor("nombreProfesorTest", "apellidoProfesorTest", "iosfaTest4"));
		
	}

	@AfterEach
	void tearDown() throws Exception {
		
		personaVacio = null;
		personaLleno = null;
		alumno = null;
		profesor = null;
		alumno = null;
		profesor = null;
		
	}

	@Test
	void testHashCode() {
		
		Persona personaTest = new Persona();
		personaTest.setNombre("nombreTest");
		personaTest.setApellido("apellidoTest");
		assertEquals(personaLleno.hashCode(), personaTest.hashCode());
	}

	@Test
	void testPersonaConstructorVacio() {
		
		assertEquals(personaVacio.getNombre(), "");
		assertEquals(personaVacio.getApellido(), "");
	}

	@Test
	void testPersonaConstructorlleno() {
		
		assertEquals(personaLleno.getNombre(), "nombreTest");
		assertEquals(personaLleno.getApellido(), "apellidoTest");
	}

	@Test
	void testToString() {
		
		assertEquals(personaLleno.toString(), "\nNombre -> nombreTest,apellido -> apellidoTest");
		
	}

	@Test
	void testEqualsPersonaTrue() {
		
		Persona personaTest = new Persona();
		personaTest.setNombre("nombreTest");
		personaTest.setApellido("apellidoTest");
		assertTrue(personaLleno.equals(personaTest));
	}
	
	@Test
	void testEqualsPersonaFalse() {
		
		Persona personaTest = new Persona();
		personaTest.setNombre("nombre");
		personaTest.setApellido("apellido");
		assertFalse(personaLleno.equals(personaTest));
	}
	
	@Test
	void testEqualsAlumnoTrue() {
		
		Persona alumnoTest = new Alumno("nombreAlumnoTest", "apellidoAlumnoTest", 1);
		assertTrue(alumnoTest.equals(alumno));
	}
	
	@Test
	void testEqualsAlumnoFalse() {
		
		Persona alumnoTest = new Alumno("nombre", "apellido", 1);
		assertFalse(alumnoTest.equals(alumno));
	}
	
	@Test
	void testEqualsProfesorTrue() {
		
		Persona profesorTest = new Profesor("nombreProfesorTest", "apellidoProfesorTest", "iosfaTest");
		assertTrue(profesorTest.equals(profesor));
	}
	
	@Test
	void testEqualsProfesorFalse() {
		
		Persona profesorTest = new Profesor("nombre", "apellido", "iosfa");
		assertFalse(profesorTest.equals(profesor));
	}
	
	@Test
	void testListaAdd() {
	
		Persona personaPrueba = new Alumno();
		assertTrue(listaPersonas.add(personaPrueba));
	
	}
	
	@Test
	void testSetAdd() {
		
		Persona personaPrueba = new Alumno();
		assertFalse(setPersonas.add(personaPrueba));
		
	}
	

}
