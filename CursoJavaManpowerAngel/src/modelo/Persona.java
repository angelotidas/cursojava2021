package modelo;


public class Persona {

	private String nombre;
	private String apellido;
	
	public Persona() {
		super();
		this.nombre = "";
		this.apellido = "";
	}
	
	public Persona(String nombre, String apellido) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getApellido() {
		return apellido;
	}
	
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public int hashCode()
	{
		return nombre.hashCode() + apellido.hashCode();
	}
	
	public String toString()
	{
		return "\nNombre -> " + nombre + ",apellido -> "  + apellido;
	}
	
	public boolean equals(Object object)
	{
		boolean esIgual = false;
				
		if(object instanceof Persona)
		{
			Persona persona = (Persona) object;
			if(this.nombre.equalsIgnoreCase(persona.getNombre()) && this.apellido.equalsIgnoreCase(persona.getApellido()) )
			{
				esIgual = true;
			}
		}
				
		return esIgual;
	}
	
}
