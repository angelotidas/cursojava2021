package modelo;

public class Profesor extends Persona {

	private String iosfa;
	
	public Profesor() {
	}

	public Profesor(String nombre, String apellido, String iosfa) {
		super(nombre, apellido);
		this.iosfa = iosfa;
	}

	public String getIosfa() {
		return iosfa;
	}

	public void setIosfa(String iosfa) {
		this.iosfa = iosfa;
	}
	
	public int hasCode()
	{
		return super.hashCode() + this.iosfa.hashCode();
	}
	
	public String toString()
	{
		return "\nNombre -> " + this.getNombre() + ", apellido -> "  + this.getApellido() + ", IOSFA -> " + this.iosfa;
	}
	
	public boolean equals(Object object)
	{
		boolean esIgual = false;
				
		if(object instanceof Profesor)
		{
			Profesor profesor = (Profesor) object;
			if(this.getNombre().equalsIgnoreCase(profesor.getNombre()) && this.getApellido().equalsIgnoreCase(profesor.getApellido()) && profesor.getIosfa().equals(this.iosfa) )
			{
				esIgual = true;
			}
		}
				
		return esIgual;
	}


}
