package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



public class ConnectionTest {

	public ConnectionTest()
	{
		
	}
	
	public static void main(String[] args) {

		try 
		{
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/manpower", "root", "root");
			
			Statement sentencia = conexion.createStatement();
			
			ResultSet resultado = sentencia.executeQuery("SELECT alu_id, alu_apellido, alu_nombre, alu_estudios, alu_linkgit FROM alumnos");
			
			while(resultado.next())
			{
				System.out.println("Apellido: " + resultado.getString("alu_apellido") + ", nombre: " + resultado.getString("alu_nombre"));
			}
		} 
		catch (ClassNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (SQLException e) 
		{	
			e.printStackTrace();
		}

	}

}
