package string.util;

public class StringUtil {

	public static boolean containsDobleSpace(String str)
	{
		int contador = 0;
		boolean tieneDosEspacios = false;
		
		for (int i = 0; i < str.length() && tieneDosEspacios == false; i++) {
			
			if(str.charAt(i) == ' ')
			{
				contador++;
			}
			if(contador == 2)
			{
				tieneDosEspacios = true;
			}
		}
		return tieneDosEspacios;
	}
	
	public static boolean containsNumber(String str)
	{
		String numeros = "0123456789";
		boolean tieneNumeros = false;
		
		for (int i = 0; i < str.length() && tieneNumeros == false; i++) {
			
			if(numeros.contains(str.charAt(i) + ""))
			{
				tieneNumeros = true;
			}
			
		}
		
		return tieneNumeros;
	}
	
}
