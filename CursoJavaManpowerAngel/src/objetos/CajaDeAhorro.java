package objetos;

public class CajaDeAhorro extends Cuenta {

	
	private float interes;
	
	public CajaDeAhorro() {
	}

	public CajaDeAhorro(int numero, float saldo, float interes) {
		super(numero, saldo);
		this.interes = interes;
	}

	public float getInteres() {
		return interes;
	}

	public void setInteres(float interes) {
		this.interes = interes;
	}

	@Override
	public void debitar(float saldo) {
		if(saldo <= getSaldo())
		{
			setSaldo(getSaldo() - saldo);
		}
	}
	
	public boolean equals(Object object)
	{
		return object instanceof CajaDeAhorro 
				&& super.equals(object)
				&& interes == ((CajaDeAhorro)object).getInteres();
	}
	
	public int hashCode()
	{
		return super.hashCode() + (int)(interes * 100);
	}
	
	public String toString()
	{
		return super.toString() + " , inter�s = " + interes;
	}

}
