package objetos.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import objetos.CajaDeAhorro;
import objetos.Cuenta;
import objetos.CuentaCorriente;
import objetos.exception.CuentaException;

class CuentaTest {

	Cuenta cajaDeAhorroVacia;
	Cuenta cajaDeAhorroLlena;
	Cuenta cuentaCorrienteVacia;
	List<Cuenta> listaCuentas; //Permite duplicados ordenados
	Set<Cuenta> setCuentas; //No permite duplicados y no tiene orden
	
	
	@BeforeEach
	void setUp() throws Exception {
		
		//Upcast
		cajaDeAhorroVacia = new CajaDeAhorro();
		cajaDeAhorroLlena = new CajaDeAhorro(20, 2000, 0.2f);
		cuentaCorrienteVacia = new CuentaCorriente();
		//Los objetos s�lo se crean con NEW
		
		listaCuentas = new ArrayList<>();
		listaCuentas.add(new CajaDeAhorro());
		listaCuentas.add(new CuentaCorriente());
		
		listaCuentas.add(new CajaDeAhorro(1, 10, 0.1f));
		listaCuentas.add(new CajaDeAhorro(2, 20, 0.2f));
		listaCuentas.add(new CajaDeAhorro(3, 30, 0.3f));
		
		listaCuentas.add(new CuentaCorriente(10, 1000, 100));
		listaCuentas.add(new CuentaCorriente(20, 2000, 200));
		listaCuentas.add(new CuentaCorriente(30, 3000, 300));
		
		setCuentas = new HashSet<>();
		setCuentas.add(new CajaDeAhorro());
		setCuentas.add(new CuentaCorriente());
		
		setCuentas.add(new CajaDeAhorro(1, 10, 0.1f));
		setCuentas.add(new CajaDeAhorro(2, 20, 0.2f));
		setCuentas.add(new CajaDeAhorro(3, 30, 0.3f));
		
		setCuentas.add(new CuentaCorriente(10, 1000, 100));
		setCuentas.add(new CuentaCorriente(20, 2000, 200));
		setCuentas.add(new CuentaCorriente(30, 3000, 300));
		
	}

	@AfterEach
	void tearDown() throws Exception {
		
		cajaDeAhorroVacia = null;
		cajaDeAhorroLlena = null;
		cuentaCorrienteVacia = null;
		listaCuentas = null;
		setCuentas = null;
		Cuenta.setCantidadDeCuentas(0);
	}
	
	@Test
	void testCajadeAhorroEqualsTrue()
	{
		Cuenta cajaDeAhorro = new CajaDeAhorro();
		assertTrue(cajaDeAhorroVacia.equals(cajaDeAhorro));
	}
	
	@Test
	void testCajadeAhorroEqualsFalse()
	{
		Cuenta cajaDeAhorro = new CajaDeAhorro();
		cajaDeAhorro.acreditar(100);
		assertFalse(cajaDeAhorroVacia.equals(cajaDeAhorro));
	}
	
	@Test
	void listaAdd()
	{
		Cuenta cuentaPrueba = new CajaDeAhorro();
		assertTrue(listaCuentas.add(cuentaPrueba));
		System.out.println("listaCuentas -> " + listaCuentas);
	}
	
	@Test
	void setAdd()
	{
		Cuenta cuentaPrueba = new CajaDeAhorro();
		assertFalse(setCuentas.add(cuentaPrueba));
		System.out.println("setCuentas -> " + setCuentas);
	}
	
	@Test
	void listaEqualsContainsTrue()
	{
		Cuenta cuentaPrueba = new CajaDeAhorro();
		assertTrue(listaCuentas.contains(cuentaPrueba));
	}
	
	@Test
	void listaEqualsContainsFalse()
	{
		Cuenta cuentaPrueba = new CajaDeAhorro();
		cuentaPrueba.acreditar(100);
		assertFalse(listaCuentas.contains(cuentaPrueba));
	}
	
	@Test
	void testCantidadDeCuentas() {
		assertEquals(19, Cuenta.getCantidadDeCuentas());
	}

	//Constructor vac�o
	@Test
	void testCuenta() {
		assertEquals(10, cajaDeAhorroVacia.getNumero());
		assertEquals(1000.0f, cajaDeAhorroVacia.getSaldo(), 0.01);
	}

	//Constructor con par�metros
	@Test
	void testCuentaIntFloat() {
		assertEquals(20, cajaDeAhorroLlena.getNumero());
		assertEquals(2000.0f, cajaDeAhorroLlena.getSaldo(), 0.01);
	}

	//M�todo para a�adir saldo
	@Test
	void testAcreditar() {
		cajaDeAhorroVacia.acreditar(100);
		assertEquals(1100.0f, cajaDeAhorroVacia.getSaldo(), 0.01);
	}

	//M�todo para retirar saldo
	@Test
	void testDebitarEnCajaAhorro() {
		try {
			cajaDeAhorroVacia.debitar(100);
			assertEquals(900.0f, cajaDeAhorroVacia.getSaldo(), 0.01);
		} 
		catch (CuentaException e) {
			assertTrue(false); // Pintar de rojo
			e.printStackTrace();
		}
		
	}
	
	@Test
	void testDebitarEnCuentaCorriente() {
		try {
			cuentaCorrienteVacia.debitar(200);
			assertEquals(1000.0f, cajaDeAhorroVacia.getSaldo(), 0.01);
		} 
		catch (CuentaException e) {
			
			assertFalse(true);
			e.printStackTrace();
		}
		
		
	}

}
