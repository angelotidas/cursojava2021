package objetos;

import objetos.exception.CuentaException;

public abstract class Cuenta {

	//Atributos
	private static int cantidadDeCuentas;
	private int numero;
	private float saldo;
	
	
	
	public Cuenta() {
		super();
		this.numero = 10;
		this.saldo = 1000;
		cantidadDeCuentas++;
	}
	public Cuenta(int numero, float saldo) {
		super();
		this.numero = numero;
		this.saldo = saldo;
		cantidadDeCuentas++;
	}
	
	public int getNumero() {
		return numero;
	}
	
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public float getSaldo() {
		return saldo;
	}
	
	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}

	public static int getCantidadDeCuentas() {
		return cantidadDeCuentas;
	} 
	
	public static void setCantidadDeCuentas(int cantidadDeCuentas) {
		Cuenta.cantidadDeCuentas = cantidadDeCuentas;
	}
	
	public void acreditar(float saldo)
	{
		this.saldo += saldo;
	}
	
	public abstract void debitar(float saldo) throws CuentaException;
	
	public boolean equals(Object obj)
	{
		//Establecemos reglas de negocio
		//Downcast
		boolean esIgual = false;
		
		if(obj instanceof Cuenta)
		{
			Cuenta cuenta = (Cuenta) obj;
			if(this.numero == cuenta.getNumero() && this.saldo == cuenta.getSaldo())
			{
				esIgual = true;
			}
		}
		
		return esIgual;
	}
	
	public int hashCode()
	{
		return numero + (int)saldo;
	}
	
	public String toString()
	{
		return "\nN�mero " + numero + ", saldo "  + saldo;
	}
	
	
}
