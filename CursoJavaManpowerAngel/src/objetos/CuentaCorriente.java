package objetos;

import objetos.exception.CuentaException;

public class CuentaCorriente extends Cuenta {

	private float descubierto;
	
	public CuentaCorriente() {
		super(10, 1000);
		this.descubierto = 150;
	}

	public CuentaCorriente(int numero, float saldo, float descubierto) {
		super(numero, saldo);
		this.descubierto = descubierto;
	}
	
	

	public float getDescubierto() {
		return descubierto;
	}

	public void setDescubierto(float descubierto) {
		this.descubierto = descubierto;
	}

	@Override
	public void debitar(float saldo) throws CuentaException {
		
		if(saldo < (getSaldo() + descubierto))
		{
			setSaldo(getSaldo() - saldo); 
		}
		else
		{
			throw new CuentaException("Saldo insufiente");
		}

	}

}
