package modulo5_pantalla;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import javax.swing.JButton;

public class Pantalla_String {

	private JFrame frame;
	private JTextField textField;
	private JLabel lblMayusc;
	private JLabel lblMinus;
	private JLabel lblSust;
	private JLabel lblResulMayu;
	private JLabel lblResulMinu;
	private JLabel lblResulSus;
	private JButton btnTransformar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla_String window = new Pantalla_String();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla_String() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblTitle = new JLabel("Manejo de String");
		lblTitle.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblTitle.setBounds(125, 0, 198, 50);
		frame.getContentPane().add(lblTitle);
		
		JLabel lblTextField = new JLabel("Ingrese texto: ");
		lblTextField.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblTextField.setBounds(10, 60, 107, 14);
		frame.getContentPane().add(lblTextField);
		
		textField = new JTextField();
		textField.setBounds(125, 57, 172, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		lblMayusc = new JLabel("May�suclas");
		lblMayusc.setFont(new Font("Tahoma", Font.ITALIC, 14));
		lblMayusc.setBounds(10, 96, 107, 17);
		frame.getContentPane().add(lblMayusc);
		
		lblMinus = new JLabel("Min�sculas");
		lblMinus.setFont(new Font("Tahoma", Font.ITALIC, 14));
		lblMinus.setBounds(10, 136, 107, 17);
		frame.getContentPane().add(lblMinus);
		
		lblSust = new JLabel("Sutitucion (O->2)");
		lblSust.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSust.setBounds(9, 174, 108, 17);
		frame.getContentPane().add(lblSust);
		
		lblResulMayu = new JLabel("");
		lblResulMayu.setOpaque(true);
		lblResulMayu.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblResulMayu.setBackground(Color.ORANGE);
		lblResulMayu.setBounds(125, 96, 172, 17);
		frame.getContentPane().add(lblResulMayu);
		
		lblResulMinu = new JLabel("");
		lblResulMinu.setOpaque(true);
		lblResulMinu.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblResulMinu.setBackground(Color.ORANGE);
		lblResulMinu.setBounds(125, 136, 172, 17);
		frame.getContentPane().add(lblResulMinu);
		
		lblResulSus = new JLabel("");
		lblResulSus.setOpaque(true);
		lblResulSus.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblResulSus.setBackground(new Color(255, 200, 0));
		lblResulSus.setBounds(125, 174, 172, 17);
		frame.getContentPane().add(lblResulSus);
		
		btnTransformar = new JButton("Transformar");
		btnTransformar.setBounds(177, 215, 91, 23);
		frame.getContentPane().add(btnTransformar);
		btnTransformar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				lblResulMayu.setText(textField.getText().toUpperCase());
				lblResulMinu.setText(textField.getText().toLowerCase());
				lblResulSus.setText(textField.getText().replace('o', '2'));
			}
		});
	}
}
