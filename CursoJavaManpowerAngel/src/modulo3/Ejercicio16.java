package modulo3;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {
		
		int tabla = 0;
		Scanner sc = new Scanner(System.in);
		
		do {
			System.out.println("Indique n�mero para obetener su tabla: ");
			tabla = sc.nextInt();
		} while (tabla < 0 || tabla > 10);
		
		System.out.println("Tabla del " + tabla);
		
		for (int i = 0; i <= 10; i++) {
			System.out.println(tabla + "x" + i + "=" +tabla * i);
		}
		
		sc.close();
	}

}
