package modulo3;

import java.util.Scanner;

public class Ejercicio10 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int aux = 0;
		
		System.out.print("Primer n�mero: ");
		int numero1 = sc.nextInt();
		
		System.out.print("Segundo n�mero: ");
		int numero2 = sc.nextInt();
		
		System.out.print("Tercer n�mero: ");
		int numero3 = sc.nextInt();
		
		
		sc.close();
		
		if (numero1 > numero2 && numero1 > numero3) 
		{
			aux = numero1;
		}
		else if (numero2 > numero1 && numero2 > numero3) 
		{
			aux = numero2;
		} 
		else 
		{
			aux = numero3;
		}
		
		System.out.println("El n�mero m�s alto es " + aux);

	}

}
