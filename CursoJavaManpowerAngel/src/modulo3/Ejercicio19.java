package modulo3;

import java.util.Random;

public class Ejercicio19 {

	public static void main(String[] args) {
		
		final int NUM = 10;
		Random random = new Random();
		int aux = 0;
		int total = 0;
		float contador = 0f;
		while(contador < NUM)
		{
			aux = random.nextInt(100) + 1;
			System.out.println("N�mero: " + aux);
			total += aux;
			contador++;
		}
		System.out.println("Total: " + total + ", MEDIA -> " + total / contador);
	}

}
