package modulo3;

import java.util.Scanner;

public class Ejercicio08y09 {

	public static void main(String[] args) {
		
		int piedra = 1;
		int papel = 2;
		int tijera = 3;
		String[] nombres = {"Piedra", "Papel", "Tijera"};
		Scanner sc = new Scanner(System.in);
		System.out.print("Elija su jugada[1-Piedra, 2-Papel, 3-Tijera]: ");
		int jugada1 = sc.nextInt();
		
		System.out.print("Elija su jugada[1-Piedra, 2-Papel, 3-Tijera]: ");
		int jugada2 = sc.nextInt();
			
		sc.close();
		
		if (jugada1 == piedra && jugada2 == tijera || jugada1 == papel && jugada2 == piedra || jugada1 == tijera && jugada2 == papel) 
		{
			System.out.println("Gana jugador1 -> " + nombres[jugada1-1] + " gana a " + nombres[jugada2 -1]);
		}
		else if(jugada1 == jugada2)
		{
			System.out.println("Empate.");
		}
		else 
		{
			System.out.println("Gana jugador2 -> " + nombres[jugada2-1] + " gana a " + nombres[jugada1 -1]);
		}

	}

}
