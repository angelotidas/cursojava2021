package modulo3;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		
		System.out.print("Indique un tipo de veh�culo [a,b,c]: ");
		Scanner sc = new Scanner(System.in);
		
		char opcion = sc.nextLine().charAt(0);
		sc.close();
		
	    switch (opcion) 
	    {
	        case 'a':  
	        	System.out.println("4 ruedas y un motor"); 
	        	break;
	        case 'b':  
	        	System.out.println("4 ruedas, un motor, cierre centralizado y aire");
	        	break;
	        case 'c':  
	        	System.out.println("4 ruedas, un motor, cierre centralizado, aire, airbag");
	        	break;
	        default:  
	        	System.out.println("Vehiculo no reconocido"); 
	        	break;
	    }

	}

}
