package modulo3;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {
		
		System.out.println("Indique una categor�a [a , b , c ]: ");
		Scanner sc = new Scanner(System.in);
		String opcion;
		
		do {
			opcion = sc.nextLine();
		} 
		while (!opcion.equalsIgnoreCase("a") && !opcion.equalsIgnoreCase("b") && !opcion.equalsIgnoreCase("c"));

		sc.close();
		
		if(opcion.equalsIgnoreCase("a"))
		{
			System.out.println("Hijo");
		}
		else if(opcion.equalsIgnoreCase("b"))
		{
			System.out.println("Padres");
		}
		else if(opcion.equalsIgnoreCase("c"))
		{
			System.out.println("Abuelos");
		}
	}

}
