package modulo3;

import java.util.Scanner;

public class Ejercicio21 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		String categoria = "";
		int sueldo = 0;
		double sueldoFinal = 0d;
		int tiempoAcumulado = 0;
		
		do {
			System.out.print("Indique categor�a [A,B,C]: ");
			categoria = sc.nextLine();
		} while (!categoria.equals("A") && !categoria.equals("B") && !categoria.equals("C"));
		
		System.out.print("Indique sueldo: ");
		sueldo = Integer.parseInt(sc.nextLine());
		
		System.out.println("Indique antig�edad: ");
		tiempoAcumulado = Integer.parseInt(sc.nextLine());
		
		sc.close();
		
		switch (categoria) {
		
			case "A":
				
				if(tiempoAcumulado >= 1 && tiempoAcumulado <= 5)
				{
					sueldoFinal = (sueldo * 1.05) + 1000;
				}
				else if (tiempoAcumulado >= 6 && tiempoAcumulado <= 10) 
				{
					sueldoFinal = (sueldo * 1.10) + 1000;
				}
				else
				{
					sueldoFinal = (sueldo * 1.30) + 1000;
				}
				
				break;
			
			case "B":
				
				if(tiempoAcumulado >= 1 && tiempoAcumulado <= 5)
				{
					sueldoFinal = (sueldo * 1.05) + 2000;
				}
				else if (tiempoAcumulado >= 6 && tiempoAcumulado <= 10) 
				{
					sueldoFinal = (sueldo * 1.10) + 2000;
				}
				else
				{
					sueldoFinal = (sueldo * 1.30) + 2000;
				}
				
				break;
				
			case "C":
				
				if(tiempoAcumulado >= 1 && tiempoAcumulado <= 5)
				{
					sueldoFinal = (sueldo * 1.05) + 3000;
				}
				else if (tiempoAcumulado >= 6 && tiempoAcumulado <= 10) 
				{
					sueldoFinal = (sueldo * 1.10) + 3000;
				}
				else
				{
					sueldoFinal = (sueldo * 1.30) + 3000;
				}
				
				break;
	
	
			default:
				break;
		}

		System.out.println("El sueldo final es de " + sueldoFinal);
	}

}
