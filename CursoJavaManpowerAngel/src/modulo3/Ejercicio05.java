package modulo3;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int numero;
		
		do {
			System.out.print("Indique n�mero: ");
			numero = sc.nextInt();
		} 
		while(numero <= 0);
		
		sc.close();
		
		if(numero == 1)
		{
			System.out.println("Obtienes la medalla de oro");
		}
		else if(numero == 2)
		{
			System.out.println("Obtienes la medalla de plata");
		}
		else if(numero == 3)
		{
			System.out.println("Obtienes la medalla de bronce");
		}
		else
		{
			System.out.println("Sigue participando");
		}
	}

}
