package modulo3;

import java.util.Scanner;

public class Ejercicio17 {

	public static void main(String[] args) {
		
		int tabla = 0;
		int sumaPares = 0;
		Scanner sc = new Scanner(System.in);
		
		do {
			System.out.println("Indique n�mero para obetener su tabla: ");
			tabla = sc.nextInt();
		} while (tabla < 0 || tabla > 10);
		
		System.out.println("Tabla del " + tabla);
		
		for (int i = 1; i <= 10; i++) {
			System.out.println(tabla + "x" + i + "=" + tabla * i);
			
			//Suma de los pares con if
			/*
			if(i % 2 == 0)
			{
				sumaPares += tabla * i;
			}
			*/
		}
		
		//Suma de los pares con algoritmo
		for (int i = 0; i <= 10; i+=2) {
			sumaPares += tabla * i;
		}
		
		System.out.println("La suma de los pares es " + sumaPares);
		
		sc.close();

	}

}
