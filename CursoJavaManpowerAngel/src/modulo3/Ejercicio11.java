package modulo3;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		
		char a = 'a';
		char e = 'e';
		char i = 'i';
		char o = 'o';
		char u = 'u';
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Indique una letra");
		char letra = sc.nextLine().charAt(0);
		
		sc.close();
		
		if(letra == a || letra == e || letra == i || letra == o || letra == u)
		{
			System.out.println("Se ha introducido una vocal.");
		}
		else
		{
			System.out.println("Se ha introducido una consonante.");
		}
	}

}
