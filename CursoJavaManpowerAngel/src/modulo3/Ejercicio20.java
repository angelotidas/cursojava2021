package modulo3;

import java.util.Random;

public class Ejercicio20 {

	public static void main(String[] args) {
		final int NUM = 10;
		Random random = new Random();
		int aux = 0;
		int numeroMinimo = Integer.MAX_VALUE;
		int numeroMaximo = Integer.MIN_VALUE;
		float contador = 0f;
		while(contador < NUM)
		{
			aux = random.nextInt(100) + 1;
			System.out.println("N�mero: " + aux);
			if(aux >= numeroMaximo)
			{
				numeroMaximo = aux;
			}
			if(aux <= numeroMinimo)
			{
				numeroMinimo = aux;
			}
			contador++;
		}
		System.out.println("M�nimo -> " + numeroMinimo + ", m�xmimo -> " + numeroMaximo);

	}

}
