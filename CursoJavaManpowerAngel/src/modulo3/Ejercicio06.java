package modulo3;

import java.util.Scanner;

public class Ejercicio06 {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int numero;
		
		do {
			System.out.print("Indique n�mero: ");
			numero = sc.nextInt();
		} 
		while(numero < 0);
		
		sc.close();
		
		if(numero == 0)
		{
			System.out.println("Jard�n de infantes");
		}
		else if(numero >= 1 && numero <= 6)
		{
			System.out.println("Primaria");
		}
		else if(numero >= 7 && numero <= 12)
		{
			System.out.println("Secundaria");
		}

	}
}
