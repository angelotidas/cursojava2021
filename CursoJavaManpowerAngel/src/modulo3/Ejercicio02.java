package modulo3;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {
		
		System.out.println("Indique un n�mero: ");
		Scanner sc = new Scanner(System.in);
		int numero = sc.nextInt();
		
		if(numero % 2 == 0)
		{
			System.out.println("El n�mero es par.");
		}
		else
		{
			System.out.println("El n�mero es impar.");
		}
		sc.close();

	}

}
