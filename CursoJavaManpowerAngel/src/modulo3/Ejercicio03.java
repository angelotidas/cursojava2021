package modulo3;

public class Ejercicio03 {

	public static void main(String[] args) {
		
		String[] meses = {"Enero", "Febrero","Marzo", "Abril","Mayo", "Junio", "Julio", "Agosto","Septiembre", "Octubre", "Noviembre" , "Diciembre"};
		
		for (int i = 0; i < meses.length; i++) {
			
			System.out.print(meses[i] + " tiene ");
			
			if(i < 7 && i % 2 == 0)
			{
				System.out.println(31 + " d�as.");
			}
			else if(i >= 7 && i % 2 != 0)
			{
				System.out.println(31 + " d�as.");
			}
			else if(i == 1)
			{
				System.out.println(28 + " d�as.");
			}
			else
			{
				System.out.println(30 + " d�as.");
			}
		}
		
	}
}
