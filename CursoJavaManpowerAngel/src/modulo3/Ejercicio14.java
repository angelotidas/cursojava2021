package modulo3;

import java.util.Scanner;

public class Ejercicio14 {

	public static void main(String[] args) {
		
		System.out.print("Indique un n�mero: ");
		Scanner sc = new Scanner(System.in);
		
		int posicion = sc.nextInt();
		sc.close();
		
	    switch (posicion) 
	    {
	        case 1:  
	        	System.out.println("Obtienes la medalla de oro"); 
	        	break;
	        case 2:  
	        	System.out.println("Obtienes la medalla de plata");
	        	break;
	        case 3:  
	        	System.out.println("Obtienes la medalla de bronce");
	        	break;
	        default:  
	        	System.out.println("Sigue participando"); 
	        	break;
	    }

	}

}
