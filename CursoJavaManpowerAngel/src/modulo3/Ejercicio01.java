package modulo3;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		System.out.println("Indique primera nota: ");
		Scanner sc = new Scanner(System.in);
		float nota1 = sc.nextFloat();
		
		System.out.println("Indique segunda nota: ");
		float nota2 = sc.nextFloat();
		
		System.out.println("Indique tercera nota: ");
		float nota3 = sc.nextFloat();
		
		sc.close();
		
		System.out.println("La media es " + (nota1 + nota2 + nota3)/3);
		
		if((nota1 + nota2 + nota3)/3 >= 7)
		{
			System.out.println("Aprobado");
		}
		else
		{
			System.out.println("Suspenso");
		}
	}

}
