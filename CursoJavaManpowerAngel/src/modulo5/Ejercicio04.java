package modulo5;

public class Ejercicio04 {

	public static void main(String[] args) {
		
		String ejemplo = "esto es una prueba de la clase String";
		String vocales = "aeiou";
		int contadorVocales = 0;
		int contadorConsonantes = 0;
		
		for (int i = 0; i < ejemplo.length(); i++) {
			
			if(vocales.contains(ejemplo.charAt(i) + ""))
			{
				contadorVocales++;
			}
			else if(!vocales.contains(ejemplo.charAt(i) + "") && ejemplo.charAt(i) != ' ')
			{
				contadorConsonantes++;
			}
		}
		
		System.out.println("N�mero de vocales -> " + contadorVocales);
		System.out.println("N�mero de consonantes-> " + contadorConsonantes);
		
		
	}

}
