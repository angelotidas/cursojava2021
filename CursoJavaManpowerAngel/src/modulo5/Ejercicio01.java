package modulo5;

public class Ejercicio01 {

	public static void main(String[] args) {

		String ejemplo = "Hola mundo";
		
		System.out.println(ejemplo.toUpperCase());
		System.out.println(ejemplo.toLowerCase());
		System.out.println(ejemplo.replaceAll("o", "2"));
	}

}
