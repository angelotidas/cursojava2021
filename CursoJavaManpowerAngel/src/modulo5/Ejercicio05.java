package modulo5;

public class Ejercicio05 {

	public static void main(String[] args) {
		
		String correo = "gcasas1972@gmail.com";
		boolean encontrado = false;
		
		for (int i = 0; i < correo.length() && encontrado == false; i++) {
			
			if(correo.charAt(i) == '@')
			{
				System.out.println("Caracter '@' encontrado en la posici�n " + i);
			}
		}
		
		String[] subStrings = correo.split("@");
		
		System.out.println("Primer subtring -> " + subStrings[0]);
		System.out.println("Segundo subtring -> " + subStrings[1]);
		
	}

}
