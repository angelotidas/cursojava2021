package modulo5;

public class Ejercicio06 {

	public static void main(String[] args) {
		String ejemplo = "3st0 3s un4 pru3ba d3 l4 cl4s3 Str1ng";
		String numeros = "0123456789";
		boolean contieneNumeros = false;
		
		for (int i = 0; i < ejemplo.length() && contieneNumeros == false; i++) {
			
			if(numeros.contains(ejemplo.charAt(i) + ""))
			{
				contieneNumeros = true;
				System.out.println("La cadena contiene n�meros");
			}
		}
		
		if(contieneNumeros == false)
		{
			System.out.println("La cadena no contiene n�meros");
		}
	}
	

}
