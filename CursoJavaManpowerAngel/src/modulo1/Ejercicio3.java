package modulo1;

public class Ejercicio3 {

	public static void main(String[] args) {
		
		System.out.println("Tecla de escape\t\t significado");
		System.out.println("\\n\t\t\t significa nueva l�nea\r\n"
				+ "\\t\t\t\t significa un tab de espacio\r\n"
				+ "\\�\t\t\t es para poner � (comillas dobles) dentro del texto por ejemplo\r\n"
				+ "�Belencita�\r\n"
				+ "\\\\\t\t\t se utiliza para escribir la \\ dentro del texto, por ejemplo \\algo\\\r\n"
				+ "\\�\t\t\t se utiliza para las �(comilla simple) para escribir por ejemplo �Princesita�");

	}

}
