package modulo2;

public class Ejercicio2 {

	public static void main(String[] args) {
		byte bmin = -128;
		byte bmax = 127;
		// reemplazar el 0 por el valor que corresponda en todos los caso
		short shortMin = (short) (Math.pow(2, 15) *(-1)); //-32768;
		short shortMax = (short) (Math.pow(2, 15) -1); //32767;
		int integerMin = (int) (Math.pow(2, 31) *(-1)); //-2147483648;
		int integerMax = (int) (Math.pow(2, 31)  -1); //2147483647;
		long longMin = (long) (Math.pow(2, 63) *(-1)); //-9223372036854775808
		long longMax = (long) (Math.pow(2, 63)  -1); //9223372036854775807
		System.out.println("tipo\tminimo\tmaximo");
		System.out.println("....\t......\t......");
		System.out.println("\nsbyte\t" + bmin + "\t" + bmax);
		System.out.println("\nshort\t" + shortMin + "\t" + shortMax);
		System.out.println("\nint\t" + integerMin + "\t" + integerMax);
		System.out.println("\nlong\t" + longMin + "\t" + longMax);
		
		//F�rmula general para el m�ximo (2^ (bits que ocupa el tipo de dato -1) -1)
		//F�rmula general para el m�nimo (2^ (bits que ocupa el tipo de dato -1) *(-1)
	}

}
