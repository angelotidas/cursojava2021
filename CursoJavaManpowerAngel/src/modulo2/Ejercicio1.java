package modulo2;

public class Ejercicio1 {

	public static void main(String[] args) {
		
		byte byteMin = -128;
		byte byteMax = 127;
		
		int i = 1357;

		float numero1 = 0;
		int numero2 = 3;
		float f = numero1/0;
		double raiz = Math.sqrt(-1);
		byte b = (byte)i;
		
		System.out.println("f-> " + f);
		System.out.println("raiz-> " + raiz);
		System.out.println("b-> " + b);
		
		int numeroAleatorio = 3;
		System.out.println("N�mero cualquiera -> " + numeroAleatorio++); //Primero imprime, despu�s asigna
		System.out.println("N�mero cualquiera -> " + ++numeroAleatorio); //Primero asigna, despu�s imprime
	}

}
