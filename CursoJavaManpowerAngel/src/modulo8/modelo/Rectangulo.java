package modulo8.modelo;

public class Rectangulo extends Figura {

	private float base;
	private float altura;
	
	public Rectangulo() {
		
		maximaSuperficie = 1000f;

	}

	public Rectangulo(String nombre) {
		super(nombre);
		maximaSuperficie = 1000f;
	}

	public Rectangulo(String nombre, float base, float altura) {
		super(nombre);
		this.base = base;
		this.altura = altura;
		maximaSuperficie = 1000f;
	}
	

	@Override
	public float calcularPerimetro() {
		return (2 * base) + (2 * altura);
	}

	@Override
	public float calcularSuperficie() {
		return base * altura;
	}

	@Override
	public String getValores() {
		return "Base -> " + base + ", altura -> " + altura;
	}

	public float getBase() {
		return base;
	}

	public void setBase(float base) {
		this.base = base;
	}

	public float getAltura() {
		return altura;
	}

	public void setAltura(float altura) {
		this.altura = altura;
	}
	
	public String toString()
	{
		return getNombre();
	}
	
	public boolean equals(Object object)
	{
		return object instanceof Rectangulo 
				&& base == ((Rectangulo)object).getBase()
				&& altura == ((Rectangulo)object).getAltura();
	}
	
	public int hashCOde()
	{
		return (int)(getNombre().hashCode() + base + altura);
	}

}
