package modulo8.modelo;

public class Cuadrado extends Figura {

	private float lado;
	
	public Cuadrado() {
		
		maximaSuperficie = 1000f;
	}

	public Cuadrado(String nombre, float lado) {
		super(nombre);
		this.lado = lado;
		maximaSuperficie = 1000f;
	}
	
	@Override
	public float calcularPerimetro() {
		return lado * 4;
	}

	@Override
	public float calcularSuperficie() {
		return lado * lado;
	}

	@Override
	public String getValores() {
		return lado + "";
	}

	public float getLado() {
		return lado;
	}

	public void setLado(float lado) {
		this.lado = lado;
	}
	
	public String toString()
	{
		return getNombre();
	}
	
	public boolean equals(Object object)
	{
		return object instanceof Cuadrado && this.lado == ((Cuadrado)object).getLado();
	}
	
	public int hashCOde()
	{
		return (int)(getNombre().hashCode() + lado);
	}

}
