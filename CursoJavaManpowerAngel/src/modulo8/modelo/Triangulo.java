package modulo8.modelo;

public class Triangulo extends Figura {

	private float ladoA;
	private float ladoB;
	private float ladoC;
	
	
	public Triangulo() {
		maximaSuperficie = 1000f;
	}

	public Triangulo(String nombre, float ladoA, float ladoB, float ladoC) {
		super(nombre);
		this.ladoA = ladoA;
		this.ladoB = ladoB;
		this.ladoC = ladoC;
		maximaSuperficie = 1000f;
	}

	@Override
	public float calcularPerimetro() {
		
		return ladoA + ladoB + ladoC;
	}

	@Override
	public float calcularSuperficie() {
		
		float semiPerimetro = getSemiperimetro(ladoA, ladoB, ladoC);
		
		return (float) Math.sqrt(semiPerimetro * (semiPerimetro - ladoA) *(semiPerimetro - ladoB) *(semiPerimetro - ladoC));
	}

	@Override
	public String getValores() {
		
		return "Lado A -> " + ladoA + ", lado B -> " + ladoB + ", lado C -> " + ladoC;
	}
	
	public boolean equals(Object object)
	{
		return object instanceof Triangulo
				&& ladoA == ((Triangulo)object).getLadoA()
				&& ladoB == ((Triangulo)object).getLadoB()
				&& ladoC == ((Triangulo)object).getLadoC();
	}
	
	private static float getSemiperimetro(float ladoA, float ladoB, float ladoC)
	{
		return (ladoA + ladoB + ladoC) / 2;
	}

	public float getLadoA() {
		return ladoA;
	}

	public void setLadoA(float ladoA) {
		this.ladoA = ladoA;
	}

	public float getLadoB() {
		return ladoB;
	}

	public void setLadoB(float ladoB) {
		this.ladoB = ladoB;
	}

	public float getLadoC() {
		return ladoC;
	}

	public void setLadoC(float ladoC) {
		this.ladoC = ladoC;
	}
	
	

}
