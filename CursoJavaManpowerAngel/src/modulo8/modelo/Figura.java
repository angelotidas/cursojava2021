package modulo8.modelo;

public abstract class Figura {

	protected static float maximaSuperficie;
	private String nombre;
	
	public Figura()
	{
		super();
		nombre = "Figura";
	}
	
	public Figura(String nombre)
	{
		super();
		this.nombre = nombre;
	}

	public static float getMaximaSuperficie() {
		return maximaSuperficie;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public abstract float calcularPerimetro();
	
	public abstract float calcularSuperficie();
	
	public abstract String getValores();
	
	public boolean equals(Object object)
	{
		return object instanceof Figura;
	}
	
	public int hashCode()
	{
		return (int) (nombre.hashCode() + maximaSuperficie);
	}
	
	public String toString()
	{
		return nombre;
	}
	
	
}
