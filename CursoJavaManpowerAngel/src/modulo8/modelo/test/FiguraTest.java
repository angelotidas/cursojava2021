package modulo8.modelo.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import modulo8.modelo.Circulo;
import modulo8.modelo.Cuadrado;
import modulo8.modelo.Figura;
import modulo8.modelo.Rectangulo;
import modulo8.modelo.Triangulo;

class FiguraTest {

	//Lote de pruebas
	Figura cuadrado;
	Figura rectangulo;
	Figura triangulo;
	Figura circulo;
	Figura cuadradoVacio;
	Figura rectanguloVacio;
	Figura trianguloVacio;
	Figura circuloVacio;
	List<Figura> listaFormas;
	Set<Figura> setFormas;
	
	@BeforeEach
	void setUp() throws Exception {
		
		cuadrado = new Cuadrado("Nombre_cuadrado", 5f);
		rectangulo = new Rectangulo("Nombre_rectangulo", 6f, 5f);
		triangulo = new Triangulo("Nombre_triangulo", 3f, 4f, 5f);
		circulo = new Circulo("Nombre_circulo", 6f);
		
		cuadradoVacio = new Cuadrado();
		rectanguloVacio = new Rectangulo();
		trianguloVacio = new Triangulo();
		circuloVacio = new Circulo();
		
		listaFormas = new ArrayList<Figura>();
		listaFormas.add(new Cuadrado("Nombre_cuadrado", 5f));
		listaFormas.add(new Rectangulo("Nombre_rectangulo", 6f, 5f));
		listaFormas.add(new Triangulo("Nombre_triangulo", 3f, 4f, 5f));
		listaFormas.add(new Circulo("Nombre_circulo", 6f));
		listaFormas.add(new Cuadrado());
		listaFormas.add(new Rectangulo());
		listaFormas.add(new Triangulo());
		listaFormas.add(new Circulo());
		
		setFormas = new HashSet<Figura>();
		setFormas.add(new Cuadrado("Nombre_cuadrado", 5f));
		setFormas.add(new Rectangulo("Nombre_rectangulo", 6f, 5f));
		setFormas.add(new Triangulo("Nombre_triangulo", 3f, 4f, 5f));
		setFormas.add(new Circulo("Nombre_circulo", 6f));
		setFormas.add(new Cuadrado());
		setFormas.add(new Rectangulo());
		setFormas.add(new Triangulo());
		setFormas.add(new Circulo());
	}

	@AfterEach
	void tearDown() throws Exception {
		
		cuadrado = null;
		rectangulo = null;
		triangulo = null;
		circulo = null;
		cuadradoVacio = null;
		rectanguloVacio = null;
		trianguloVacio = null;
		circuloVacio = null;
		listaFormas = null;
		setFormas = null;
	}

	//Comportamiento de la lista
	@Test
	void listaAdd()
	{
		Cuadrado cuadradoPrueba = new Cuadrado();
		assertTrue(listaFormas.add(cuadradoPrueba));
	}
	
	//Comportamiento del set
	@Test
	void setAdd()
	{
		Cuadrado cuadradoPrueba = new Cuadrado();
		assertFalse(setFormas.add(cuadradoPrueba));
	}
	
	//Constructor vac�o
	@Test
	void testFigura() {
		assertEquals("Figura", cuadradoVacio.getNombre());
		assertEquals("Figura", rectanguloVacio.getNombre());
		assertEquals("Figura", trianguloVacio.getNombre());
		assertEquals("Figura", circuloVacio.getNombre());
	}
	
	//Constructor con par�metros
	@Test
	void testFiguraParametros() {
		
		assertEquals("Nombre_cuadrado", cuadrado.getNombre());
		assertEquals("Nombre_rectangulo", rectangulo.getNombre());
		assertEquals("Nombre_triangulo", triangulo.getNombre());
		assertEquals("Nombre_circulo", circulo.getNombre());
		
	}
	
	//M�todo toString()
	@Test
	void testFiguraString() {
		
		assertEquals("Nombre_cuadrado", cuadrado.toString());
		assertEquals("Nombre_rectangulo", rectangulo.toString());
		assertEquals("Nombre_triangulo", triangulo.toString());
		assertEquals("Nombre_circulo", circulo.toString());
		
		assertEquals("Figura", cuadradoVacio.toString());
		assertEquals("Figura", rectanguloVacio.toString());
		assertEquals("Figura", trianguloVacio.toString());
		assertEquals("Figura", circuloVacio.toString());
	}

	//M�todo equals positivo
	@Test
	void testEqualsObjectTrue() {
		
		Cuadrado cuadradoAux = new Cuadrado("Nombre_cuadrado", 5f);
		Rectangulo rectanguloAux = new Rectangulo("Nombre_rectangulo", 6f, 5f);
		Triangulo triangulodAux = new Triangulo("Nombre_triangulo", 3f, 4f, 5f);
		Circulo circuloAux = new Circulo("Nombre_circulo", 6f);
		
		assertTrue(cuadradoAux.equals(cuadrado));
		assertTrue(rectanguloAux.equals(rectangulo));
		assertTrue(triangulodAux.equals(triangulo));
		assertTrue(circuloAux.equals(circulo));
		
	}
	
	//M�todo equals negativo
	@Test
	void testEqualsObjectFalse() {
		
		Cuadrado cuadradoAux = new Cuadrado("Nombre_cuadrado_false", 8f);
		Rectangulo rectanguloAux = new Rectangulo("Nombre_rectangulo_false", 5f, 6f);
		Triangulo triangulodAux = new Triangulo("Nombre_triangulo_false", 9f, 10f, 11f);
		Circulo circuloAux = new Circulo("Nombre_circulo_false", 2f);
		
		assertFalse(cuadradoAux.equals(cuadrado));
		assertFalse(rectanguloAux.equals(rectangulo));
		assertFalse(triangulodAux.equals(triangulo));
		assertFalse(circuloAux.equals(circulo));
		
	}
	
	//Calcular per�metros
	@Test
	void testPerimetros()
	{
		assertEquals(20f, cuadrado.calcularPerimetro());
		assertEquals(22f, rectangulo.calcularPerimetro());
		assertEquals(12f, triangulo.calcularPerimetro());
		assertEquals(37.699112f, circulo.calcularPerimetro());
	}
	
	//Calcular �reas
	@Test
	void testAreas()
	{
		assertEquals(20f, cuadrado.calcularSuperficie());
		assertEquals(30f, rectangulo.calcularSuperficie());
		assertEquals(6f, triangulo.calcularSuperficie());
		assertEquals(113.097336f, circulo.calcularSuperficie());
	}

}
