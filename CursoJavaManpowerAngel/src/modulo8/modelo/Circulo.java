package modulo8.modelo;

public class Circulo extends Figura {

	private float radio;
	
	public Circulo() {
		
		maximaSuperficie = 1000f;
	}

	public Circulo(String nombre, float radio) {
		super(nombre);
		this.radio = radio;
		maximaSuperficie = 1000f;
	}
	
	

	public float getRadio() {
		return radio;
	}

	public void setRadio(float radio) {
		this.radio = radio;
	}

	@Override
	public float calcularPerimetro() {
		
		return (float) (2 * Math.PI * radio);
	}

	@Override
	public float calcularSuperficie() {

		return (float) (Math.PI * Math.pow(radio, 2));
	}

	@Override
	public String getValores() {
		return "Radio -> " + radio;
	}
	
	public String toString()
	{
		return getNombre();
	}
	
	public boolean equals(Object object)
	{
		return object instanceof Circulo && radio == ((Circulo)object).getRadio();
	}
	
	public int hashCOde()
	{
		return (int)(getNombre().hashCode() + radio);
	}

}
