package date.util;

import static org.junit.jupiter.api.Assertions.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DateUtilTest {

	//Lote de pruebas
	Date fecha;
	
	@BeforeEach
	void setUp() throws Exception {
		
		//Singleton
		Calendar calendar = Calendar.getInstance();
		calendar.set(1994, Calendar.NOVEMBER, 17);
		fecha = calendar.getTime();
		
	}

	@AfterEach
	void tearDown() throws Exception {
		
		fecha = null;
	}

	@Test
	void testGetAnio() {
		
		assertEquals(1994, DateUtil.getAnio(fecha));
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha);
		System.out.println("Fecha -> " + fecha);
		System.out.println("Calendario -> " + calendar);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyy-mm-dd-EEE");
		System.out.println("Fecha formateada -> " + sdf.format(fecha));
	}

	@Test
	void testGetMes() {
		assertEquals(Calendar.NOVEMBER, DateUtil.getMes(fecha));
	}

	@Test
	void testGetDia() {
		assertEquals(17, DateUtil.getDia(fecha));
	}

	@Test
	void testIsFinDeSemana() {
		assertFalse(DateUtil.isFinDeSemana(fecha));
	}

	@Test
	void testIsDiaDeSemana() {
		assertTrue(DateUtil.isDiaDeSemana(fecha));
	}

	@Test
	void testGetDiaDeSemana() {
		assertEquals(5, DateUtil.getDiaDeSemana(fecha));
	}

	@Test
	void testAsDate() throws ParseException {
		//TODO
	}

	@Test
	void testAsCalendar() {
		//TODO
	}

	@Test
	void testAsString() {
		//TODO
	}

}
