package date.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	public DateUtil()
	{
		
	}
	
	
	public static int getAnio(Date fecha)
	{
		Calendar calendar = dateToCalendar(fecha);
		
		return calendar.get(Calendar.YEAR);
	}
	
	public static int getMes(Date fecha)
	{
		Calendar calendar = dateToCalendar(fecha);
		
		return calendar.get(Calendar.MONTH);
	}
	
	public static int getDia(Date fecha)
	{
		Calendar calendar = dateToCalendar(fecha);
		
		return calendar.get(Calendar.DAY_OF_MONTH);
	}
	
	public static boolean isFinDeSemana(Date fecha)
	{
		Calendar calendar = dateToCalendar(fecha);
		
		return calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
				|| calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
		
	}
	
	public static boolean isDiaDeSemana(Date fecha)
	{
		Calendar calendar = dateToCalendar(fecha);
		
		return calendar.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY
				|| calendar.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY
				|| calendar.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY
				|| calendar.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY
				|| calendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY;
	}
	
	public static int getDiaDeSemana(Date fecha)
	{
		Calendar calendar = dateToCalendar(fecha);
		
		return calendar.get(Calendar.DAY_OF_WEEK);
			
	}
	
	public static Date asDate(String pattern, String fecha) throws ParseException
	{
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.parse(fecha);
	}
	
	public static Calendar asCalendar(String pattern, String fecha) throws ParseException
	{
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		Date dateAux = sdf.parse(fecha);
		return dateToCalendar(dateAux);
	}
	
	public static String asString(String pattern, Date fecha)
	{
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(fecha);
	}
	
	//M�todo para obtener un objeto Calendar a partir de date
	private static Calendar dateToCalendar(final Date date)
	{
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar;
	}
	
}
