package patrones.factory;

public class Papel extends PiedraPapelTijeraFactory {

	public Papel() {
		numero = 1;
		nombre = "Papel";
	}

	@Override
	public boolean isMe(int numero) {
		
		return numero == 1;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory elemento) {
		// piedra (0) -> GANA
		// papel (1) -> EMPATA
		// tijera (2) -> PIERDE
				
		int resultado = 0;
				
		switch (elemento.numero) 
		{
			case 0:
						
				resultado = 1;
						
				break;
						
			case 1:
						
				resultado = 0;
						
				break;
						
			case 2:
					
				resultado = -1;
						
				break;
			
			default:
						
				break;
		}
		return resultado;
	}

}
