package patrones.factory;

public class Tijera extends PiedraPapelTijeraFactory {

	public Tijera() {
		numero = 2;
		nombre = "Tijera";
	}

	@Override
	public boolean isMe(int numero) {
		return numero == 2;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory elemento) {
		// piedra (0) -> PIERDE
		// papel (1) ->  GANA
		// tijera (2) -> EMPATA
				
		int resultado = 0;
				
		switch (elemento.numero) 
		{
			case 0:
						
				resultado = -1;
				
				break;
						
			case 1:
						
				resultado = 1;
						
				break;
						
			case 2:
					
				resultado = 0;
						
				break;
			
			default:
						
				break;
		}
		return resultado;
	}

}
