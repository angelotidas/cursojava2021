package patrones.factory;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PiedraPapelTijeraFactoryTest {

	//Lote de pruebas
	PiedraPapelTijeraFactory piedra;
	PiedraPapelTijeraFactory papel;
	PiedraPapelTijeraFactory tijera;
	
	
	@BeforeEach
	void setUp() throws Exception {
		
		piedra = new Piedra();
		papel = new Papel();
		tijera = new Tijera();
	}

	@AfterEach
	void tearDown() throws Exception {
		
		piedra = null;
		papel = null;
		tijera = null;
		
	}

	@Test
	void testCompararPapelGanaAPiedra() {

		assertEquals(1, papel.comparar(piedra));

	}
	
	@Test
	void testCompararPiedraGanaATijera() {

		assertEquals(1, piedra.comparar(tijera));

	}
	
	@Test
	void testCompararTijeraGanaAPapel() {

		assertEquals(1, tijera.comparar(papel));

	}
	
	@Test
	void testCompararTijeraPierdeContraPiedra() {

		assertEquals(-1, tijera.comparar(piedra));

	}
	
	@Test
	void testCompararPapelPierdeContraTijera() {

		assertEquals(-1, papel.comparar(tijera));

	}
	
	@Test
	void testCompararPiedraPierdeContraPapel() {

		assertEquals(-1, piedra.comparar(papel));

	}
	
	@Test
	void testGetInstancePiedra() {

		assertTrue(PiedraPapelTijeraFactory.getInstance(0) instanceof Piedra);

	}
	
	@Test
	void testGetInstancePapel() {

		assertTrue(PiedraPapelTijeraFactory.getInstance(1) instanceof Papel);

	}
	
	@Test
	void testGetInstanceTijera() {

		assertTrue(PiedraPapelTijeraFactory.getInstance(2) instanceof Tijera);

	}

}
