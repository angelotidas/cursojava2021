package patrones.factory;

public class Piedra extends PiedraPapelTijeraFactory {

	public Piedra() {
		
		numero = 0;
		nombre = "Piedra";
	}

	@Override
	public boolean isMe(int numero) {
		
		return numero == 0;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory elemento) {
		// piedra (0) -> EMPATA
		// papel (1) -> PIERDE
		// tijera (2) -> GANA
		
		int resultado = 0;
		
		switch (elemento.numero) 
		{
			case 0:
				
				resultado = 0;
				
				break;
				
			case 1:
				
				resultado = -1;
				
				break;
				
			case 2:
			
				resultado = 1;
				
				break;
	
			default:
				
				break;
		}
		
		return resultado;
	}

}
