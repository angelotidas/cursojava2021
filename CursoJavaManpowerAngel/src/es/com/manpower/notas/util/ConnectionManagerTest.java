package es.com.manpower.notas.util;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ConnectionManagerTest {

	//Lote de pruebas
	Connection conexion;
	
	@BeforeEach
	void setUp() throws ClassNotFoundException, SQLException
	{
		//1- Levantar el driver
		Class.forName("com.mysql.cj.jdbc.Driver");
		//2 - Conexi�n
		conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/manpower", "root", "root");
		ConnectionManager.conectar();
	}
	
	@AfterEach
	void tearDown() throws Exception
	{
		conexion = null;
		ConnectionManager.desconectar();
	}
	
	@Test
	void testConectar() {
		
		try 
		{
			ConnectionManager.conectar();
			assertTrue(ConnectionManager.getConnection().isValid(1000));
		} 
		catch (Exception e) 
		{
			assertTrue(false);
			e.printStackTrace();
		}	
	}
	
	@Test
	void testDesconectar() {
		
		try 
		{
			ConnectionManager.desconectar();
			assertTrue(ConnectionManager.getConnection().isClosed());
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}

	@Test
	void testGetConnection() throws SQLException {
		assertFalse(ConnectionManager.getConnection().isClosed());
	}

}
