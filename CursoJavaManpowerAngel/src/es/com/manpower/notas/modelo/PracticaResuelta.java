package es.com.manpower.notas.modelo;

public class PracticaResuelta implements Model {

	private int codigo;
	private int nota;
	private String observaciones;
	
	public PracticaResuelta() {
		super();
	}
	
	public PracticaResuelta(int codigo, int nota, String observaciones) {
		super();
		this.codigo = codigo;
		this.nota = nota;
		this.observaciones = observaciones;
	}
	
	public int getCodigo() {
		return codigo;
	}
	
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	
	public int getNota() {
		return nota;
	}
	
	public void setNota(int nota) {
		this.nota = nota;
	}
	
	public String getObservaciones() {
		return observaciones;
	}
	
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	//TODO crear equals y hashCode
	
	public boolean equals(Object object)
	{
		return object instanceof PracticaResuelta &&
				((PracticaResuelta)object).getCodigo() == codigo;
	}
	
	public int hashCode()
	{
		return 0;
	}
	
	
}
