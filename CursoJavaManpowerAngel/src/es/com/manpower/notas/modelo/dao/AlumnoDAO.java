package es.com.manpower.notas.modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import es.com.manpower.notas.modelo.Alumno;
import es.com.manpower.notas.modelo.Model;
import es.com.manpower.notas.modelo.dao.selectStrategy.SelectStrategy;
import es.com.manpower.notas.util.ConnectionManager;


public class AlumnoDAO implements DAO {

	public AlumnoDAO()
	{
		
	}
	
	@Override
	public void agregar(Model agregarModel) throws SQLException {
		
		//Casteamos 
		Alumno alumno = (Alumno)agregarModel;
		
		//Conexi�n a la base de datos
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConnection();
		
		//Preparamos la sentencia con la INTERFAZ
		StringBuilder stringBuilderSQL = new StringBuilder("INSERT INTO alumnos (ALU_NOMBRE , ALU_APELLIDO , ALU_ESTUDIOS , ALU_LINKGIT) VALUES");
		stringBuilderSQL.append("(?,?,?,?)");
		
		PreparedStatement sentencia = conexion.prepareStatement(stringBuilderSQL.toString());
		
		//A�adimos los valores correspondientes a cada par�metro
		sentencia.setString(1, alumno.getNombre());
		sentencia.setString(2, alumno.getApellido());
		sentencia.setString(3, alumno.getEstudios());
		sentencia.setString(4, alumno.getLinkRespositorio());
		
		
		//Ejecutamos la sentencia
		sentencia.executeUpdate();
		
		//Cerramos la conexi�n con la BBDD
		ConnectionManager.desconectar();
	}

	@Override
	public void modificar(Model modificarModel) throws SQLException {
		
		//Casteamos 
		Alumno alumno = (Alumno)modificarModel;
		
		//Conexi�n a la base de datos
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConnection();
		
		//Preparamos la sentencia con la INTERFAZ
		StringBuilder stringBuilderSQL = new StringBuilder("UPDATE alumnos ");
		stringBuilderSQL.append("SET ALU_NOMBRE = ?, ALU_APELLIDO = ?, ALU_ESTUDIOS = ?, ALU_LINKGIT = ? "); 
		stringBuilderSQL.append("WHERE ALU_ID = ?");
		
		PreparedStatement sentencia = conexion.prepareStatement(stringBuilderSQL.toString());
		
		//A�adimos los valores correspondientes a cada par�metro
		sentencia.setString(1, alumno.getNombre());
		sentencia.setString(2, alumno.getApellido());
		sentencia.setString(3, alumno.getEstudios());
		sentencia.setString(4, alumno.getLinkRespositorio());
		sentencia.setInt(5, alumno.getCodigo());
		
		//Ejecutamos la sentencia
		sentencia.executeUpdate();
		
		//Cerramos la conexi�n con la BBDD
		ConnectionManager.desconectar();
	}

	@Override
	public void eliminar(Model eliminarModel) throws SQLException {
		
		//Casteamos 
		Alumno alumno = (Alumno)eliminarModel;
		
		//Conexi�n a la base de datos
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConnection();
		
		//Preparamos la sentencia con la INTERFAZ
		StringBuilder stringBuilderSQL = new StringBuilder("DELETE FROM alumnos ");
		stringBuilderSQL.append("WHERE ALU_ID = ?");

		
		PreparedStatement sentencia = conexion.prepareStatement(stringBuilderSQL.toString());
		
		//A�adimos los valores correspondientes a cada par�metro
		sentencia.setInt(1, alumno.getCodigo());
		
		//Ejecutamos la sentencia
		sentencia.executeUpdate();
		
		//Cerramos la conexi�n con la BBDD
		ConnectionManager.desconectar();
	}

	@Override
	public List<Model> leer(Model model) throws SQLException {

		List<Model> listaAlumnos = new ArrayList<>();
		
		//Casteamos 
		Alumno alumno = (Alumno)model;
		
		//Conexi�n a la base de datos
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConnection();
		
		/*
		//Preparamos la sentencia con la INTERFAZ
		StringBuilder stringBuilderSQL = new StringBuilder("SELECT ALU_NOMBRE, ALU_APELLIDO, ALU_ESTUDIOS, ALU_LINKGIT ");
		stringBuilderSQL.append("FROM alumnos");
		
		//TODO Utilizaci�n del patr�n Strategy
		
		if(alumno.getCodigo() > 0)
		{
			stringBuilderSQL.append(" WHERE ALU_ID = ?");
		}
		
		PreparedStatement sentencia = conexion.prepareStatement(stringBuilderSQL.toString());
		
		//A�adimos los valores correspondientes a cada par�metro
		sentencia.setInt(1, alumno.getCodigo());
		
		//Ejecutamos la sentencia
		ResultSet resultado = sentencia.executeQuery();
		
		
		//Recorremos el resultado de la sentencia
		while (resultado.next()) 
		{
			listaAlumnos.add(new Alumno(resultado.getInt("ALU_ID"), 
										resultado.getString("ALU_NOMBRE"),
										resultado.getString("ALU_APELLIDO"),
										resultado.getString("ALU_ESTUDIOS"),
										resultado.getString("ALU_LINKGIT")));
			
		}
		
		
		resultado.close();
		*/
		
		PreparedStatement sentencia = conexion.prepareStatement(SelectStrategy.getSql(alumno).toString());
		//Ejecutamos la sentencia
		ResultSet resultado = sentencia.executeQuery();
		
		//Recorremos el resultado de la sentencia
		while (resultado.next()) 
		{
			listaAlumnos.add(new Alumno(resultado.getInt("ALU_ID"), 
										resultado.getString("ALU_NOMBRE"),
										resultado.getString("ALU_APELLIDO"),
										resultado.getString("ALU_ESTUDIOS"),
										resultado.getString("ALU_LINKGIT")));
		}
		
		ConnectionManager.desconectar();
		
		return listaAlumnos;
	}

}
