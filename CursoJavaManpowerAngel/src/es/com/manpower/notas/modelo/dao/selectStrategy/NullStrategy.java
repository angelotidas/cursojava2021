package es.com.manpower.notas.modelo.dao.selectStrategy;

public class NullStrategy extends SelectStrategy {

	public NullStrategy() {
		
		this.isUltimo = false;
		this.tengoWhere = false;
	}

	@Override
	public String getCondicion() {
		//No hay condición porque el SQL no tiene where
		return "";
	}

	@Override
	public boolean isMe() {
		
		this.isUltimo = alumnoAux == null;
		return alumnoAux == null;
	}

}
