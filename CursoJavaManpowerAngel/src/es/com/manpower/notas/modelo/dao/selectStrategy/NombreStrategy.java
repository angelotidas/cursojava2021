package es.com.manpower.notas.modelo.dao.selectStrategy;

public class NombreStrategy extends SelectStrategy {

	public NombreStrategy() {
	}

	@Override
	public String getCondicion() {

		StringBuilder stringBuilder = new StringBuilder(" WHERE ALU_NOMBRE LIKE '%");
		stringBuilder.append(alumnoAux.getNombre());
		stringBuilder.append("%'");
		return stringBuilder.toString();
	}

	@Override
	public boolean isMe() {
		tengoWhere = alumnoAux.getNombre() != null
				&& !alumnoAux.getNombre().isEmpty(); 
		
		return alumnoAux.getNombre() != null
				&& !alumnoAux.getNombre().isEmpty();
	}

}
