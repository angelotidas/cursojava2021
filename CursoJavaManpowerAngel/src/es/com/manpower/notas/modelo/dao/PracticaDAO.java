package es.com.manpower.notas.modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import es.com.manpower.notas.modelo.Model;
import es.com.manpower.notas.modelo.Practica;
import es.com.manpower.notas.util.ConnectionManager;

public class PracticaDAO implements DAO {
	
	public PracticaDAO()
	{
		
	}

	@Override
	public void agregar(Model agregarModel) throws ClassNotFoundException, SQLException {
		
		//Casteamos
		Practica practica = (Practica) agregarModel;
		
		//Conexi�n a la base de datos
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConnection();
		
		//Preparamos la sentencia con la INTERFAZ
		StringBuilder stringBuilderSQL = new StringBuilder("INSERT INTO practicas (PRAC_NOMBRE) VALUES");
		stringBuilderSQL.append("(?)");
		
		PreparedStatement sentencia = conexion.prepareStatement(stringBuilderSQL.toString());
		
		//A�adimos los valores correspondientes a cada par�metro
		sentencia.setString(1, practica.getNombre());
		
		//Ejecutamos la sentencia
		sentencia.executeUpdate();
		
		//Cerramos la conexi�n con la BBDD
		ConnectionManager.desconectar();
			
	}
		
	

	@Override
	public void modificar(Model modificarModel) throws ClassNotFoundException, SQLException {

		//Casteamos 
		Practica practica = (Practica) modificarModel;
				
		//Conexi�n a la base de datos
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConnection();
				
		//Preparamos la sentencia con la INTERFAZ
		StringBuilder stringBuilderSQL = new StringBuilder("UPDATE practicas ");
		stringBuilderSQL.append("SET PRAC_NOMBRE = ? "); 
		stringBuilderSQL.append("WHERE PRAC_ID = ?");
		
		PreparedStatement sentencia = conexion.prepareStatement(stringBuilderSQL.toString());
		
		//A�adimos los valores correspondientes a cada par�metro
		sentencia.setString(1, practica.getNombre());
		sentencia.setInt(2, practica.getCodigo());

		//Ejecutamos la sentencia
		sentencia.executeUpdate();
		
		//Cerramos la conexi�n con la BBDD
		ConnectionManager.desconectar();
		
	}

	@Override
	public void eliminar(Model eliminarModel) throws ClassNotFoundException, SQLException {
		
		//Casteamos 
		Practica practica = (Practica) eliminarModel;
						
		//Conexi�n a la base de datos
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConnection();
						
		//Preparamos la sentencia con la INTERFAZ
		StringBuilder stringBuilderSQL = new StringBuilder("DELETE FROM practicas ");
		stringBuilderSQL.append("WHERE PRAC_ID = ?");
				
		PreparedStatement sentencia = conexion.prepareStatement(stringBuilderSQL.toString());
		
		//A�adimos los valores correspondientes a cada par�metro
		sentencia.setInt(1, practica.getCodigo());

		//Ejecutamos la sentencia
		sentencia.executeUpdate();
				
		//Cerramos la conexi�n con la BBDD
		ConnectionManager.desconectar();
		
	}

	@Override
	public List<Model> leer(Model model) throws ClassNotFoundException, SQLException {
		
		List<Model> listaPracticas = new ArrayList<>();
		
		//Casteamos 
		Practica practica = (Practica) model;
								
		//Conexi�n a la base de datos
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConnection();
								
		//Preparamos la sentencia con la INTERFAZ
		StringBuilder stringBuilderSQL = new StringBuilder("SELECT PRAC_NOMBRE FROM practicas");
		
		if(practica.getCodigo() > 0)
		{
			stringBuilderSQL.append("WHERE PRAC_ID = ?");
		}
		
		
		PreparedStatement sentencia = conexion.prepareStatement(stringBuilderSQL.toString());
		
		//A�adimos los valores correspondientes a cada par�metro
		sentencia.setInt(1, practica.getCodigo());
		
		//Ejecutamos la sentencia
		ResultSet resultado = sentencia.executeQuery();
		
		//Recorremos el resultado de la sentencia
		while (resultado.next()) 
		{
			listaPracticas.add(new Practica(resultado.getInt("PRAC_ID"), 
										resultado.getString("PRAC_NOMBRE")));
					
		}
				
		resultado.close();

		ConnectionManager.desconectar();
		
		return listaPracticas;
	}

}
