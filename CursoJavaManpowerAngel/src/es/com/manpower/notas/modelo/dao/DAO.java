package es.com.manpower.notas.modelo.dao;

import java.sql.SQLException;
import java.util.List;

import es.com.manpower.notas.modelo.Model;

public interface DAO {

	//Patr�n de dise�o
	//CRUD -> Create Read Update Delete
	//Todos los m�todos son abtractos
	
	public void agregar(Model agregarModel) throws ClassNotFoundException, SQLException;
	
	public void modificar(Model modificarModel) throws ClassNotFoundException, SQLException;
	
	public void eliminar(Model eliminarModel) throws ClassNotFoundException, SQLException;
	
	public List<Model> leer(Model model) throws ClassNotFoundException, SQLException;
	
}
