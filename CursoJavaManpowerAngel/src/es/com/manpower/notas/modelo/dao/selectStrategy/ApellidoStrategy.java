package es.com.manpower.notas.modelo.dao.selectStrategy;

public class ApellidoStrategy extends SelectStrategy {

	public ApellidoStrategy() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getCondicion() {
		StringBuilder stringBuilder = new StringBuilder();
		if(tengoWhere)
		{
			stringBuilder.append(" AND ALU_APELLIDO LIKE '%");
			stringBuilder.append(alumnoAux.getApellido());
			stringBuilder.append("%'");
		}
		else
		{
			stringBuilder.append(" WHERE ALU_APELLIDO LIKE '%");
			stringBuilder.append(alumnoAux.getApellido());
			stringBuilder.append("%'");
			tengoWhere = true;
		}
		return stringBuilder.toString();
	}

	@Override
	public boolean isMe() {
		return alumnoAux.getApellido() != null
				&& !alumnoAux.getApellido().isEmpty();
	}

}
