package com.domain.modelo;

import java.util.List;

public class Alumno implements Model, Vaciable {

	private int codigo;
	private String nombre;
	private String apellido;
	private String estudios;
	private String linkRespositorio;
	private List<PracticaResuelta> practicasResueltas;
	
	public Alumno() {
		super();
	}

	public Alumno(int codigo, String nombre, String apellido, String estudios, String linkRespositorio) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.apellido = apellido;
		this.estudios = estudios;
		this.linkRespositorio = linkRespositorio;
	}

	public Alumno(int codigo) {
		super();
		this.codigo = codigo;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEstudios() {
		return estudios;
	}

	public void setEstudios(String estudios) {
		this.estudios = estudios;
	}

	public String getLinkRespositorio() {
		return linkRespositorio;
	}

	public void setLinkRespositorio(String linkRespositorio) {
		this.linkRespositorio = linkRespositorio;
	}

	public List<PracticaResuelta> getPracticasResueltas() {
		return practicasResueltas;
	}

	public void setPracticasResueltas(List<PracticaResuelta> practicasResueltas) {
		this.practicasResueltas = practicasResueltas;
	}
	
	public boolean equals(Object object)
	{
		return object instanceof Alumno &&
				((Alumno)object).getNombre().equals(nombre) &&
				((Alumno)object).getApellido().equals(apellido) &&
				((Alumno)object).getCodigo() == codigo;
	}
	
	public int hashCode()
	{
		return nombre.hashCode() + apellido.hashCode();
	}
	
	public String toString()
	{
		StringBuilder stringBuilder = new StringBuilder("C�digo: ");
		stringBuilder.append(this.codigo);
		stringBuilder.append(" - Nombre: ");
		stringBuilder.append(this.nombre);
		stringBuilder.append(" - Apellido: ");
		stringBuilder.append(this.apellido);
		stringBuilder.append(" - Estudios: ");
		stringBuilder.append(this.estudios);
		stringBuilder.append(" - Repositorio: ");
		stringBuilder.append(this.linkRespositorio);
		stringBuilder.append(" - Pr�cticas resueltas: ");
		stringBuilder.append(this.practicasResueltas);
		
		return stringBuilder.toString();
	}

	@Override
	public boolean isEmpty() {
		
		return codigo == 0 
				&& nombre == null
				&& apellido == null
				&& estudios == null
				&& linkRespositorio == null;
	}
	
}
