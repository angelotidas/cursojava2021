package com.domain.modelo.dao.selectStrategy;

public class Codigo0Strategy extends SelectStrategy {


	public Codigo0Strategy() {
		
	}

	@Override
	public String getCondicion() {
		
		StringBuilder stringBuilder = new StringBuilder(" WHERE ALU_ID = ");
		stringBuilder.append(alumnoAux.getCodigo());
		return stringBuilder.toString();
	}

	@Override
	public boolean isMe() {
		isUltimo = alumnoAux.getCodigo() > 0;
		return alumnoAux.getCodigo() > 0;
	}

}
