package com.domain.modelo.dao.selectStrategy;

import java.util.ArrayList;
import java.util.List;

import com.domain.modelo.Alumno;



public abstract class SelectStrategy {

	protected boolean isUltimo;
	protected static StringBuilder sql;
	protected static boolean tengoWhere;
	protected static Alumno alumnoAux;
	
	public SelectStrategy() {
		
	}
	
	public static String getSql(Alumno alumno)
	{
		
		sql = new StringBuilder("SELECT ALU_ID, "
				+ "ALU_NOMBRE, ALU_APELLIDO, "
				+ "ALU_ESTUDIOS, "
				+ "ALU_LINKGIT FROM alumnos");
		
		alumnoAux = alumno;
		//Conoce a todos sus hijos -> ES IMPORTANTE EL ORDEN DE LOS OBJETOS QUE SE A�ADEN
		List<SelectStrategy> strategies = new ArrayList<SelectStrategy>();
		strategies.add(new NullStrategy());
		strategies.add(new VacioStrategy());
		strategies.add(new Codigo0Strategy());
		strategies.add(new NombreStrategy());
		strategies.add(new ApellidoStrategy());
		strategies.add(new EstudiosStrategy());
		strategies.add(new LinkStrategy());
		
		for (SelectStrategy selectStrategy : strategies) {
			if(selectStrategy.isMe())
			{
				sql.append(selectStrategy.getCondicion());
			}
			if(selectStrategy.isUltimo)
			{
				return sql.toString();
			}
		}
		
		return sql.toString();
	}
	
	public abstract String getCondicion();
	
	public abstract boolean isMe();
}
