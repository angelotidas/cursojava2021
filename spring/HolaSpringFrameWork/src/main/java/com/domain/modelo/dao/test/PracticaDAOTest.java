package com.domain.modelo.dao.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.domain.modelo.Model;
import com.domain.modelo.Practica;
import com.domain.modelo.dao.DAO;
import com.domain.modelo.dao.PracticaDAO;
import com.domain.util.ConnectionManager;



class PracticaDAOTest {

	DAO practicaDAO;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConnection();
		
	    Statement consulta= con.createStatement();
	
	    String sql = "";
	    BufferedReader bf = new BufferedReader( new InputStreamReader( PracticaDAOTest.class.getResource( "PracticasCrear.sql" ).openStream() ) );
	    while ( (sql = bf.readLine()) != null ) {
	       if ( sql.trim().length() != 0 &&
	            !sql.startsWith( "--" ) ) {              
	          consulta.executeUpdate( sql );
	       }
	    }
	    ConnectionManager.desconectar();
	
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConnection();
		
	    Statement consulta= con.createStatement();
	
	    String sql = "";
	    BufferedReader bf = new BufferedReader( new InputStreamReader( PracticaDAOTest.class.getResource( "PracticasEliminar.sql" ).openStream() ) );
	    while ( (sql = bf.readLine()) != null ) {
	       if ( sql.trim().length() != 0 &&
	            !sql.startsWith( "--" ) ) {              
	          consulta.executeUpdate( sql );
	       }
	    }
	    ConnectionManager.desconectar();
		
	}

	@BeforeEach
	void setUp() throws Exception {
		
		practicaDAO = new PracticaDAO();
		
	}

	@AfterEach
	void tearDown() throws Exception {
		
		practicaDAO = null;
		
	}

	@Test
	void testAgregar() {


		try 
		{
			practicaDAO.agregar(new Practica(0,"Practica_test"));
			ConnectionManager.conectar();
			Connection conexion = ConnectionManager.getConnection();
			Statement sentencia = conexion.createStatement();
			ResultSet resultado = sentencia.executeQuery("SELECT PRAC_NOMBRE FROM practicas WHERE PRAC_NOMBRE = 'Practica_test'");
		 	resultado.next();
		 	assertEquals("Practica_test", resultado.getString("PRAC_NOMBRE"));
		 	
		 	resultado.close();
		 	ConnectionManager.desconectar();
		}
		catch (ClassNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Test
	void testModificar() {
		
		
		try 
		{
			//Leemos los datos para saber el ID
			ConnectionManager.conectar();
			Connection conexion = ConnectionManager.getConnection();
			Statement sentencia = conexion.createStatement();
			
			ResultSet resultado = sentencia.executeQuery("SELECT PRAC_ID FROM practicas WHERE PRAC_NOMBRE = 'Practica_test_modificar'");
			resultado.next();
			
			//Modificamos el objeto con los datos nuevos
			Practica practicaAux = new Practica(resultado.getInt("PRAC_ID"), "Practica_test_modificar_nuevo");
			practicaDAO.modificar(practicaAux);
			
			
			//Leemos para comprobar que todo funciona
			resultado = sentencia.executeQuery("SELECT PRAC_NOMBRE FROM practicas WHERE PRAC_NOMBRE = 'Practica_test_modificar_nuevo'");
			resultado.next();
			Practica practicaLeida = new Practica(resultado.getInt("PRAC_ID"), 
											resultado.getString("PRAC_NOMBRE"));
			
			assertEquals(practicaAux.getCodigo(), practicaLeida.getCodigo());
			assertEquals(practicaAux.getNombre(), practicaLeida.getNombre());

			
			resultado.close();
			ConnectionManager.desconectar();
		}
		catch (ClassNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	void testEliminar() {
	
		try 
		{
			//Leemos los datos para saber el ID
			ConnectionManager.conectar();
			Connection conexion = ConnectionManager.getConnection();
			Statement sentencia = conexion.createStatement();
			
			ResultSet resultado = sentencia.executeQuery("SELECT PRAC_ID FROM practicas WHERE PRAC_NOMBRE = 'Practica_test_leer2'");
			resultado.next();
			
			//Guardamos el objeto recuperado
			Practica practicaAux = new Practica(resultado.getInt("PRAC_ID"));
			practicaDAO.eliminar(practicaAux);

			//Leemos para comprobar que todo funciona
			resultado = sentencia.executeQuery("SELECT PRAC_ID FROM practicas WHERE PRAC_NOMBRE = 'Practica_test_leer2'");
		 	
			assertFalse(resultado.next());
			
		 	resultado.close();
		 	ConnectionManager.desconectar();
		}
		catch (ClassNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Test
	void testLeer() {
		try 
		{
			//Leemos los datos para saber el ID
			ConnectionManager.conectar();
			Connection conexion = ConnectionManager.getConnection();
			Statement sentencia = conexion.createStatement();
			
			ResultSet resultado = sentencia.executeQuery("SELECT ALU_ID FROM alumnos WHERE ALU_NOMBRE = 'Practica_test_leer1'");
		
			resultado.next();
			
			
			Practica practicaAux = new Practica(resultado.getInt("PRAC_ID"));
			List<Model> practicas = practicaDAO.leer(practicaAux);
			
			assertEquals("Practica_test_leer1", ((Practica)practicas.get(0)).getNombre());
			
			resultado.close();
			ConnectionManager.desconectar();
		} 
		catch (ClassNotFoundException | SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}

	}

}
