package com.domain.modelo.dao.selectStrategy.test;



import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import com.domain.modelo.Alumno;
import com.domain.modelo.dao.selectStrategy.SelectStrategy;



class SelectStrategyTest {

	//Lote de pruebas
	Alumno alumnoVacio;
	Alumno alumnoConCodigo;
	Alumno alumnoConNombre;
	Alumno alumnoConNombreYApellido;
	Alumno alumnoConEstudios;
	Alumno alumnoConNombreEstudiosYLink;
	
	
	@BeforeEach
	void setUp() throws Exception {
		
		alumnoVacio = new Alumno();
		alumnoConCodigo = new Alumno(10);
		alumnoConNombre = new Alumno(0,"Gabriel", null, null, null);
		alumnoConNombreYApellido = new Alumno(0,"Gabriel", "Casas", null, null);
		alumnoConEstudios = new Alumno(0, "Gabriel", null, "DAM", null);
		alumnoConNombreEstudiosYLink = new Alumno(0, "Gabriel", null, "DAM", "Linkgit");
		
	}

	@AfterEach
	void tearDown() throws Exception {
		
		alumnoVacio = null;
		alumnoConCodigo = null;
		alumnoConNombre = null;
		alumnoConNombreYApellido = null;
		alumnoConEstudios = null;
		alumnoConNombreEstudiosYLink = null;
		
	}
	
	@Test
	void testGetSqlVacio() {
		assertEquals("SELECT ALU_ID, "
			+ "ALU_NOMBRE, ALU_APELLIDO, "
			+ "ALU_ESTUDIOS, "
			+ "ALU_LINKGIT FROM alumnos", SelectStrategy.getSql(alumnoVacio));
	}
	
	@Test
	void testGetSqlNull() {
		assertEquals("SELECT ALU_ID, "
			+ "ALU_NOMBRE, ALU_APELLIDO, "
			+ "ALU_ESTUDIOS, "
			+ "ALU_LINKGIT FROM alumnos", SelectStrategy.getSql(null));
	}

	@Test
	void testGetSqlConCodigo() {
		assertEquals("SELECT ALU_ID, "
			+ "ALU_NOMBRE, ALU_APELLIDO, "
			+ "ALU_ESTUDIOS, "
			+ "ALU_LINKGIT FROM alumnos WHERE ALU_ID = 10", SelectStrategy.getSql(alumnoConCodigo));
	}
	
	@Test
	void testGetSqlConNombre() {
		assertEquals("SELECT ALU_ID, "
			+ "ALU_NOMBRE, ALU_APELLIDO, "
			+ "ALU_ESTUDIOS, "
			+ "ALU_LINKGIT FROM alumnos WHERE ALU_NOMBRE LIKE '%Gabriel%'", SelectStrategy.getSql(alumnoConNombre));
	}
	
	@Test
	void testGetSqlNombreYApellido() {
		StringBuilder sb = new StringBuilder("SELECT ALU_ID, "
				+ "ALU_NOMBRE, ALU_APELLIDO, "
				+ "ALU_ESTUDIOS, "
				+ "ALU_LINKGIT");
		sb.append(" FROM alumnos");
		sb.append(" WHERE ALU_NOMBRE LIKE '%Gabriel%'");
		sb.append(" AND ALU_APELLIDO LIKE '%Casas%'");
		

		assertEquals(sb.toString(), SelectStrategy.getSql(alumnoConNombreYApellido));
	}
	
	
	@Test
	void testGetSqlEstudios() {
		StringBuilder sb = new StringBuilder("SELECT ALU_ID, "
				+ "ALU_NOMBRE, ALU_APELLIDO, "
				+ "ALU_ESTUDIOS, "
				+ "ALU_LINKGIT");
		sb.append(" FROM alumnos");
		sb.append(" WHERE ALU_NOMBRE LIKE '%Gabriel%'");
		sb.append(" AND ALU_ESTUDIOS LIKE '%DAM%'");
		

		assertEquals(sb.toString(), SelectStrategy.getSql(alumnoConEstudios));
	}

	
	@Test
	void testGetSqlLinkRepo() {
		StringBuilder sb = new StringBuilder("SELECT ALU_ID, "
				+ "ALU_NOMBRE, ALU_APELLIDO, "
				+ "ALU_ESTUDIOS, "
				+ "ALU_LINKGIT");
		sb.append(" FROM alumnos");
		sb.append(" WHERE ALU_NOMBRE LIKE '%Gabriel%'"); 
		sb.append(" AND ALU_ESTUDIOS LIKE '%DAM%'");
		sb.append(" AND ALU_LINKGIT LIKE '%Linkgit%'");

		assertEquals(sb.toString(), SelectStrategy.getSql(alumnoConNombreEstudiosYLink));
	}

}
