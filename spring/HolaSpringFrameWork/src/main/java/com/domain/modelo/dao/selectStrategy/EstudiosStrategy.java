package com.domain.modelo.dao.selectStrategy;

public class EstudiosStrategy extends SelectStrategy {

	public EstudiosStrategy() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getCondicion() {
		StringBuilder stringBuilder = new StringBuilder();
		if(tengoWhere)
		{
			stringBuilder.append(" AND ALU_ESTUDIOS LIKE '%");
			stringBuilder.append(alumnoAux.getEstudios());
			stringBuilder.append("%'");
		}
		else
		{
			stringBuilder.append(" WHERE ALU_ESTUDIOS LIKE '%");
			stringBuilder.append(alumnoAux.getEstudios());
			stringBuilder.append("%'");
			tengoWhere = true;
		}
		return stringBuilder.toString();
	}

	@Override
	public boolean isMe() {
		this.tengoWhere = alumnoAux.getEstudios() != null && !alumnoAux.getEstudios().isEmpty(); 
		return alumnoAux.getEstudios() != null && !alumnoAux.getEstudios().isEmpty();
	}

}
