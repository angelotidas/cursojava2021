package com.domain.modelo.dao.selectStrategy;

public class LinkStrategy extends SelectStrategy {

	public LinkStrategy() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getCondicion() {
		
		StringBuilder stringBuilder = new StringBuilder();
		if(tengoWhere)
		{
			stringBuilder.append(" AND ALU_LINKGIT LIKE '%");
			stringBuilder.append(alumnoAux.getLinkRespositorio());
			stringBuilder.append("%'");
		}
		else
		{
			stringBuilder.append(" WHERE ALU_LINKGIT LIKE '%");
			stringBuilder.append(alumnoAux.getLinkRespositorio());
			stringBuilder.append("%'");
			tengoWhere = true;
		}
		return stringBuilder.toString();
	}

	@Override
	public boolean isMe() {
		this.tengoWhere = alumnoAux.getLinkRespositorio() != null && !alumnoAux.getLinkRespositorio().isEmpty(); 
		return alumnoAux.getLinkRespositorio() != null && !alumnoAux.getLinkRespositorio().isEmpty();
	}

}
