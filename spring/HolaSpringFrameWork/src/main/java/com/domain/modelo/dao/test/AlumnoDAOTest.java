package com.domain.modelo.dao.test;



import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;


import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.domain.modelo.Alumno;
import com.domain.modelo.Model;
import com.domain.modelo.dao.AlumnoDAO;
import com.domain.modelo.dao.DAO;
import com.domain.util.ConnectionManager;



class AlumnoDAOTest {

	DAO alumnoDAO;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConnection();
		
	    Statement consulta= con.createStatement();
	
	    String sql = "";
	    BufferedReader bf = new BufferedReader( new InputStreamReader( AlumnoDAOTest.class.getResource( "AlumnosCrear.sql" ).openStream() ) );
	    while ( (sql = bf.readLine()) != null ) {
	       if ( sql.trim().length() != 0 &&
	            !sql.startsWith( "--" ) ) {              
	          consulta.executeUpdate( sql );
	       }
	    }
	    ConnectionManager.desconectar();
		
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConnection();
		
	    Statement consulta= con.createStatement();
	
	    String sql = "";
	    BufferedReader bf = new BufferedReader( new InputStreamReader( AlumnoDAOTest.class.getResource( "AlumnosEliminar.sql" ).openStream() ) );
	    while ( (sql = bf.readLine()) != null ) {
	       if ( sql.trim().length() != 0 &&
	            !sql.startsWith( "--" ) ) {              
	          consulta.executeUpdate( sql );
	       }
	    }
	    ConnectionManager.desconectar();
	}
	
	@BeforeEach
	void setUp(){
		alumnoDAO = new AlumnoDAO();
	}
	
	@AfterEach
	void tearDown(){
		alumnoDAO = null;
	}

	@Test
	void testAgregar() {
		
		try 
		{
			alumnoDAO.agregar(new Alumno(0,"Nombre_test","Apellido_test","Estudios_test","Linkrepo_test"));
			ConnectionManager.conectar();
			Connection conexion = ConnectionManager.getConnection();
			Statement sentencia = conexion.createStatement();
			ResultSet resultado = sentencia.executeQuery("SELECT ALU_NOMBRE FROM alumnos WHERE ALU_NOMBRE = 'Nombre_test'");
		 	resultado.next();
		 	assertEquals("Nombre_test", resultado.getString("ALU_NOMBRE"));
		 	
		 	resultado.close();
		 	ConnectionManager.desconectar();
		}
		catch (ClassNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	void testModificar() {

		try 
		{
			//Leemos los datos para saber el ID
			ConnectionManager.conectar();
			Connection conexion = ConnectionManager.getConnection();
			Statement sentencia = conexion.createStatement();
			
			ResultSet resultado = sentencia.executeQuery("SELECT ALU_ID FROM alumnos WHERE ALU_NOMBRE = 'Marina_test'");
			resultado.next();
			
			//Modificamos el objeto con los datos nuevos
			Alumno alumnoAux = new Alumno(resultado.getInt("ALU_ID"), "MarinaModif_Test", "Calvo Pe�aModif_test", "FisicaModif_test", "RepoModif_test");
			alumnoDAO.modificar(alumnoAux);
			
			
			//Leemos para comprobar que todo funciona
			resultado = sentencia.executeQuery("SELECT ALU_NOMBRE, ALU_APELLIDO, ALU_ESTUDIOS, ALU_LINKGIT FROM alumnos WHERE ALU_NOMBRE = 'MarinaModif_Test'");
			resultado.next();
			Alumno alumnoLeido = new Alumno(resultado.getInt("ALU_ID"), 
											resultado.getString("ALU_NOMBRE"), 
											resultado.getString("ALU_APELLIDO"), 
											resultado.getString("ALU_ESTUDIOS"), 
											resultado.getString("ALU_LINKGIT"));
			
			assertEquals(alumnoAux.getCodigo(), alumnoLeido.getCodigo());
			assertEquals(alumnoAux.getNombre(), alumnoLeido.getNombre());
			assertEquals(alumnoAux.getApellido(), alumnoLeido.getApellido());
			assertEquals(alumnoAux.getEstudios(), alumnoLeido.getEstudios());
			assertEquals(alumnoAux.getLinkRespositorio(), alumnoLeido.getLinkRespositorio());
			
			resultado.close();
			ConnectionManager.desconectar();
		}
		catch (ClassNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Test
	void testEliminar() {
		
		try 
		{
			//Leemos los datos para saber el ID
			ConnectionManager.conectar();
			Connection conexion = ConnectionManager.getConnection();
			Statement sentencia = conexion.createStatement();
			
			ResultSet resultado = sentencia.executeQuery("SELECT ALU_ID FROM alumnos WHERE ALU_NOMBRE = 'Monsef_test'");
			resultado.next();
			
			//Guardamos el objeto recuperado
			Alumno alumnoAux = new Alumno(resultado.getInt("ALU_ID"));
			alumnoDAO.eliminar(alumnoAux);
			
			
			//Leemos para comprobar que todo funciona
			resultado = sentencia.executeQuery("SELECT ALU_ID FROM alumnos WHERE ALU_NOMBRE = 'Monsef_Test'");
			
			assertFalse(resultado.next());
			
			resultado.close();
			ConnectionManager.desconectar();
		}
		catch (ClassNotFoundException | SQLException e) 
		{
			assertTrue(false);	
			e.printStackTrace();
		}
		
	}

	@Test
	void testLeerPorCodigo() {
		
		try 
		{
			//Leemos los datos para saber el ID
			ConnectionManager.conectar();
			Connection conexion = ConnectionManager.getConnection();
			Statement sentencia = conexion.createStatement();
			
			ResultSet resultado = sentencia.executeQuery("SELECT ALU_ID FROM alumnos WHERE ALU_NOMBRE = 'Aar�n_test'");
			resultado.next();
			
			Alumno alumnoAux = new Alumno(resultado.getInt("ALU_ID"));
			List<Model> alumnos = alumnoDAO.leer(alumnoAux);
			
			assertEquals("Aar�n_test", ((Alumno)alumnos.get(0)).getNombre());
			assertEquals("S�nchez S�nchez_test", ((Alumno)alumnos.get(0)).getApellido());
			assertEquals("Desarrollo de Aplicaciones Multiplataforma_test", ((Alumno)alumnos.get(0)).getEstudios());
			assertEquals("https://github.com/Pashinian/CursoJava2021.git_test", ((Alumno)alumnos.get(0)).getLinkRespositorio());
			

			resultado.close();
			ConnectionManager.desconectar();
		
		}
		catch (ClassNotFoundException | SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}

}
