package com.domain.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.domain.modelo.Alumno;
import com.domain.modelo.dao.AlumnoDAO;

@Controller
public class IndexController {
	
	@RequestMapping("/home")
	public String goIndex()
	{
		return "Index";
	}
	
	@RequestMapping("/listado")
	public String goListado(Model model)
	{
		List<com.domain.modelo.Model> alumnos = null;
		
		AlumnoDAO alumnoDAO = new AlumnoDAO();
		
		try {
			alumnos = alumnoDAO.leer(null);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		model.addAttribute("alumnos", alumnos);
		
		
		
		return "Listado";
	}

}
