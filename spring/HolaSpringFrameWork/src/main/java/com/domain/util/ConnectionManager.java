package com.domain.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {

	private static Connection conexion;
	
	public static void conectar() {

		try 
		{
			//1- Levantar el driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			//2 - Conexi�n
			conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/manpower", "root", "root");
			
		} 
		catch (ClassNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (SQLException e) 
		{	
			e.printStackTrace();
		}

	}
	
	public static void desconectar()
	{
		try {
			conexion.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static Connection getConnection() {
		return conexion;
	}

}
